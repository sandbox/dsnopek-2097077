<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function hosting_store_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_uc_product_default_classes().
 */
function hosting_store_uc_product_default_classes() {
  $items = array(
    'hosting_product' => array(
      'name' => t('Hosting Product'),
      'module' => 'uc_product',
      'description' => t('A hosting product that will get listed in the <em>Hosting Store</em>.'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'has_body' => '1',
      'body_label' => t('Description'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function hosting_store_views_api() {
  return array(
    'api' => '2',
  );
}
