Hosting Store
=============

Combines Ubercart and a couple other modules to make an Aegir-based
store selling sites.

This has been tested to work with Aegir 1.10 and selling instances of
OpenAtrium 1.5 sites.

Dependencies
------------

 * [Aegir Ubercart Integration](https://drupal.org/project/uc_hosting)

   - Optionally, I recommend this patch:
     [#2097575](https://drupal.org/node/2097575)

 * [Hosting Profile Roles](https://drupal.org/project/hosting_profile_roles)

 * [Hosting Subdomains](https://drupal.org/project/hosting_subdomains)

 * [Ubercart](https://drupal.org/project/ubercart)

 * [Ubercart Recurring Payments](https://drupal.org/project/uc_recurring)

 * [Token](https://drupal.org/project/token)

 * [Strongarm](https://drupal.org/project/strongarm)

 * [Views](https://drupal.org/project/views)

 * [Chaos tool suite](https://drupal.org/project/ctools)

Additional setup
----------------

While this feature can help to setup a lot of stuff, there are many
things that will be specific to every store and can't be safely put in
a feature, for example: the type of sites you're selling, and how
payment will be processed.

Instead, we'll walk through performing common setup here. This will
allow you to make each of these decisions individually for your store!

### Setup your platforms for sale ###

Any existing Aegir platform can be sold! However, there are some
additional considerations:

 * Do you want any user to be able create sites on *this* platform? If
   not, edit it and give access to the "admin" client. Any platform
   which doesn't give access to a specific client will be available to
   all clients.
 
 * Do you want users to get UID 1 when they launch a site on a
   particular platform and profile? If not, visit the platform page
   and click on the "Roles" tab. Here you can give UID 1 to your
   "admin" user and give a special role to the user created for normal
   users. For example, under Open Atrium 1.5, you can give them the
   "manager" or "administrator" role.

### Set default client quota ###

By default, users can create an unlimited number of sites. Instead,
you probably want the default quota to be something relatively low
(for example: 1) and then sell quota increases.

To change the default quota, go to: "Hosting" -> "Quotas" -> "Default
Client Quotas", enter a non-zero value and click "Save".

But you probably want your "admin" client to have a much higher quota:

 1. Go to: "Hosting" -> "Quotas" -> "List Client Quotas"

 2. Click on "admin"

 3. Click on the "Edit" tab

 4. Expand the "Client quota settings" fieldset

 5. Unfortunately, using "0" doesn't work as advertised (it falls back
    on the default), so enter a very high number (for example: 100000)
 
 6. Click "Save"

### Create a Hosting Product to sell a site as subscription ###

For example, if you wanted to sell a hosted OpenAtrium site for
$100/mo.

 1. Click 'Create content'

 2. Click 'Hosting Product'

 3. Fill in the 'Name' (for example: "OpenAtrium 1.5")

 4. Fill in the 'Description' (for example: "Open Atrium is an
    intranet in a box that has group spaces to allow different teams
    to have their own conversations and collaboration.")
 
 5. Fill in 'SKU' with an arbitrary product code for your own internal
    use (for example: "OA15-STD")
 
 6. Fill in 'Sell price' - this is the initial activation cost. It can
    be the same as the recurring cost, but it doesn't have to be! If
    you want to implement a free trial this should be 0.
 
 7. Click "Save and continue"

 8. Click the "Features" tab - this is where you'll connect this to a
    platform and profile as well as configure the recurring cost.
 
 9. Select "Create a site and adjust quotas accordingly" and click
    "Add"
 
 10. If you want clients to configure their site during checkout (as
     opposed to having to click "Create content" -> "Site" later) -
     then select "Force clients to create their site on purchase"

 11. I recommend expanding the "Site Defaults (Optional)" fieldset
     and selecting a value for every field. Every that you don't
     associate will be given to the user as an option.
 
 12. Click "Save feature"

 13. Next, select "Recurring fee" and click "Add"

 14. If you want the recurring fee to be different than the activation
     cost, uncheck "Set the recurring fee amount to the same as
     selling price at the time of purchase" and set the "Recurring
     fee amount" to the recurring price.
 
 15. If you're offering a free trial, set the "Initial charge" to
     the length of the trial (for example: 30 days)

 16. Set the "Regular interval" to how often the customer will be
     charged (for example: 1 month)
 
 17. If this subscription lasts until the customer cancels, then
     check the "Unlimited rebillings" option.
 
 18. Click "Save feature"

### Setup payment ###

See the "Ubercart Recurring Payments" project page for information
about recurring payment options:

  https://drupal.org/project/uc_recurring

Install and setup the necessary modules for the one you want to use!

I won't go into the full setup here, but I can say that I've used this
successfully with PayPal WPS (Website Payments Pro). To use that, you
need to enable the following modules:

 * uc_paypal

 * uc_credit

 * uc_recurring_hosted

See the following documents for more information:

 * http://www.ubercart.org/paypal

 * https://drupal.org/documentation/modules/uc_recurring

### New user creation ###

By default Aegir disables the ability to for users to register for
their own accounts. Depending on how you want to operate your store,
you might want to allow anyone to register an account. If so:

 1. Go to: "User management" -> "User settings"

 2. Select "Visitors can create accounts and no administrator approval
    is required."
 
 3. Click "Save configuration"

An alternative, is to leave that setting alone, but allow users to
register during the checkout process:

 1. Go to: "Store administration" -> "Configuration" -> "Checkout
    settings"
 
 2. Click the "Edit" tab

 3. Make sure that "Enable anonymous checkout" is checked (it's the
    default)
 
 4. I recommend also checking "Login users when new customer accounts
    are created at checkout."
    
 5. If your are ONLY setting hosting products, I also recommend
    setting "Alternative checkout completion page" to "hosting/sites"
    so that users will immediately see the site they just bought.
 
 6. Click "Save configuration"

 7. Click the "Checkout panes" tab

 8. Expand the "Customer information settings" fieldset and specify
    what information will be necessary to create a new account.
 
 9. I recommend disabling "Allow anonymous customers to use an
    existing account's email address" and enabling "Allow anonymous
    customers to specify a new user account name"
 
 10. Click "Save configuration"

### Setup default domain and aliases ###

By default Aegir allows you to create sites with any domain name.
However, you probably want to restrict customers to creating
sub-domains on your top-level domain:

 1. Go to: "Hosting" -> "Site aliases"

 2. Fill in the "Domain used for automatic subdomain hosting" with
    your top-level domain (for example: "example.com")
 
 3. Check any of the other options if you want any default aliases.

 4. Click "Save configuration"

If you want to allow admin users to still create sites with any domain
then I recommend the following patch to hosting_subdomains:

  https://drupal.org/node/2098895

### Setup permissions ###

By default Aegir only allows authenticated users to access content.
This will need to change to allow anonymous users to see your products
and purchase!

Also, you might want to give 'aegir client' permissing to create sites
(unless you only allow them to setup sites during checkout).

 1. Go to: "User management" -> "Permissions"

 2. Check "access content" for "anonymous user"

 3. (Optional) Check "create site" for "aegir client"

 4. (Optional) Check "access task logs" for "aegir client"

 5. Click "Save permissions"

### Change site front page ###

By default, Aegir's front page is 'hosting/sites' which shows a list
of all the sites the current user has access to. However, anonymous
users don't (and shouldn't!) have access to this.

Instead they should be given some marketing page!

This feature provides a View showing all the Hosting Products sorted
according to price at 'hosting/store' - if you want, this can be your
new front page:

 1. Go to: "Site configuration" -> "Site information"

 2. In the "Default front page" field, enter "hosting/store"

 3. Click "Save configuration"

However, you probably want to redirect authenticated users to
'hosting/sites' on login. You can do this using one of the following
modules:

 * [LoginToboggan](https://drupal.org/project/logintoboggan)

 * [Login Destination](https://drupal.org/project/login_destination)

