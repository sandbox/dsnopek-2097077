<?php

/**
 * Implementation of hook_strongarm().
 */
function hosting_store_strongarm() {
  $export = array();

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'hosting_feature_alias';
  $strongarm->value = 1;
  $export['hosting_feature_alias'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'hosting_feature_client';
  $strongarm->value = 1;
  $export['hosting_feature_client'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'hosting_feature_profile_roles';
  $strongarm->value = 1;
  $export['hosting_feature_profile_roles'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uc_pane_hosting_enabled';
  $strongarm->value = 1;
  $export['uc_pane_hosting_enabled'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uc_pane_hosting_weight';
  $strongarm->value = '-9';
  $export['uc_pane_hosting_weight'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uc_product_shippable_hosting_product';
  $strongarm->value = 0;
  $export['uc_product_shippable_hosting_product'] = $strongarm;

  return $export;
}
